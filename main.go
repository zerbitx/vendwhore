package main

import (
	stdlog "log"
	"github.com/go-kit/kit/log"
	"os"
)

func main() {
	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewContext(logger).With("ts", log.DefaultTimestampUTC).With("caller", log.DefaultCaller)
		stdlog.SetFlags(0)                             // flags are handled by Go kit's logger
		stdlog.SetOutput(log.NewStdlibAdapter(logger)) // redirect anything using stdlib log to us
	}

	var transportLogger = log.NewContext(logger).With("vendoring", "suspect")

	transportLogger.Log("Booom");
}
